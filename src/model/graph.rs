// Example code that deserializes and serializes the model.
// extern crate serde;
//
// use generated_module::[object Object];
//
// fn main() {
//     let json = r#"{"answer": 42}"#;
//     let model: [object Object] = serde_json::from_str(&json).unwrap();
// }

#[derive(Serialize, Deserialize)]
pub struct GraphDocument {
    #[serde(rename = "version")]
    pub version: i64,

    #[serde(rename = "batadv")]
    pub batadv: Batadv,
}

#[derive(Serialize, Deserialize)]
pub struct Batadv {
    #[serde(rename = "directed")]
    pub directed: bool,

    #[serde(rename = "graph")]
    pub graph: Option<serde_json::Value>,

    #[serde(rename = "nodes")]
    pub nodes: Vec<Node>,

    #[serde(rename = "links")]
    pub links: Vec<Link>,
}

#[derive(Serialize, Deserialize)]
pub struct Link {
    #[serde(rename = "source")]
    pub source: usize,

    #[serde(rename = "target")]
    pub target: usize,

    #[serde(rename = "vpn")]
    pub vpn: bool,

    #[serde(rename = "tq")]
    pub tq: f64,

    #[serde(rename = "bidirect")]
    pub bidirect: bool,
}

#[derive(Serialize, Deserialize)]
pub struct Node {
    #[serde(rename = "id")]
    pub id: String,

    #[serde(rename = "node_id")]
    pub node_id: String,
}
