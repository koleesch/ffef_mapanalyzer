// Example code that deserializes and serializes the model.
// extern crate serde;
// #[macro_use]
// extern crate serde_derive;
// extern crate serde_json;
//
// use generated_module::[object Object];
//
// fn main() {
//     let json = r#"{"answer": 42}"#;
//     let model: [object Object] = serde_json::from_str(&json).unwrap();
// }

use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
pub struct NodesDocument {
    #[serde(rename = "version")]
    pub version: i64,

    #[serde(rename = "timestamp")]
    pub timestamp: String,

    #[serde(rename = "nodes")]
    pub nodes: Vec<Node>,
}

#[derive(Serialize, Deserialize)]
pub struct Node {
    #[serde(rename = "firstseen")]
    pub firstseen: String,

    #[serde(rename = "lastseen")]
    pub lastseen: String,

    #[serde(rename = "flags")]
    pub flags: Flags,

    #[serde(rename = "statistics")]
    pub statistics: Statistics,

    #[serde(rename = "nodeinfo")]
    pub nodeinfo: Nodeinfo,
}

#[derive(Serialize, Deserialize)]
pub struct Flags {
    #[serde(rename = "online")]
    pub online: bool,

    #[serde(rename = "gateway")]
    pub gateway: bool,
}

#[derive(Serialize, Deserialize)]
pub struct Nodeinfo {
    #[serde(rename = "node_id")]
    pub node_id: String,

    #[serde(rename = "network")]
    pub network: Network,

    #[serde(rename = "owner")]
    pub owner: Option<serde_json::Value>,

    #[serde(rename = "system")]
    pub system: System,

    #[serde(rename = "hostname")]
    pub hostname: String,

    #[serde(rename = "location")]
    pub location: Option<Location>,

    #[serde(rename = "software")]
    pub software: Software,

    #[serde(rename = "hardware")]
    pub hardware: Hardware,

    #[serde(rename = "vpn")]
    pub vpn: bool,
}

#[derive(Serialize, Deserialize)]
pub struct Hardware {
    #[serde(rename = "nproc")]
    pub nproc: i64,

    #[serde(rename = "model")]
    pub model: String,
}

#[derive(Serialize, Deserialize)]
pub struct Location {
    #[serde(rename = "longitude")]
    pub longitude: f64,

    #[serde(rename = "latitude")]
    pub latitude: Option<f64>,

    #[serde(rename = "altitude")]
    pub altitude: Option<i64>,
}

#[derive(Serialize, Deserialize)]
pub struct Network {
    #[serde(rename = "mac")]
    pub mac: String,

    #[serde(rename = "addresses")]
    pub addresses: Vec<String>,

    #[serde(rename = "mesh")]
    pub mesh: Mesh,

    #[serde(rename = "mesh_interfaces")]
    pub mesh_interfaces: Option<Vec<String>>,
}

#[derive(Serialize, Deserialize)]
pub struct Mesh {
    #[serde(rename = "bat0")]
    pub bat0: Bat0,
}

#[derive(Serialize, Deserialize)]
pub struct Bat0 {
    #[serde(rename = "interfaces")]
    pub interfaces: Interfaces,
}

#[derive(Serialize, Deserialize)]
pub struct Interfaces {
    #[serde(rename = "wireless")]
    pub wireless: Option<Vec<String>>,

    #[serde(rename = "tunnel")]
    pub tunnel: Option<Vec<String>>,

    #[serde(rename = "other")]
    pub other: Option<Vec<String>>,
}

#[derive(Serialize, Deserialize)]
pub struct Software {
    #[serde(rename = "autoupdater")]
    pub autoupdater: Autoupdater,

    #[serde(rename = "batman-adv")]
    pub batman_adv: BatmanAdv,

    #[serde(rename = "babeld")]
    pub babeld: Babeld,

    #[serde(rename = "fastd")]
    pub fastd: Fastd,

    #[serde(rename = "firmware")]
    pub firmware: Firmware,

    #[serde(rename = "status-page")]
    pub status_page: StatusPage,
}

#[derive(Serialize, Deserialize)]
pub struct Autoupdater {
    #[serde(rename = "enabled")]
    pub enabled: Option<bool>,

    #[serde(rename = "branch")]
    pub branch: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct Babeld {
}

#[derive(Serialize, Deserialize)]
pub struct BatmanAdv {
    #[serde(rename = "version")]
    pub version: Option<String>,

    #[serde(rename = "compat")]
    pub compat: i64,
}

#[derive(Serialize, Deserialize)]
pub struct Fastd {
    #[serde(rename = "enabled")]
    pub enabled: Option<bool>,

    #[serde(rename = "version")]
    pub version: String,
}

#[derive(Serialize, Deserialize)]
pub struct Firmware {
    #[serde(rename = "base")]
    pub base: String,

    #[serde(rename = "release")]
    pub release: String,
}

#[derive(Serialize, Deserialize)]
pub struct StatusPage {
    #[serde(rename = "api")]
    pub api: i64,
}

#[derive(Serialize, Deserialize)]
pub struct System {
    #[serde(rename = "site_code")]
    pub site_code: String,

    #[serde(rename = "domain_code")]
    pub domain_code: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct Statistics {
    #[serde(rename = "node_id")]
    pub node_id: String,

    #[serde(rename = "clients")]
    pub clients: i64,

    #[serde(rename = "rootfs_usage")]
    pub rootfs_usage: f64,

    #[serde(rename = "loadavg")]
    pub loadavg: Option<f64>,

    #[serde(rename = "memory_usage")]
    pub memory_usage: f64,

    #[serde(rename = "uptime")]
    pub uptime: f64,

    #[serde(rename = "idletime")]
    pub idletime: f64,

    #[serde(rename = "gateway")]
    pub gateway: Option<String>,

    #[serde(rename = "processes")]
    pub processes: Processes,

    #[serde(rename = "mesh_vpn")]
    pub mesh_vpn: Option<MeshVpn>,

    #[serde(rename = "traffic")]
    pub traffic: Traffic,
}

#[derive(Serialize, Deserialize)]
pub struct MeshVpn {
    #[serde(rename = "groups")]
    pub groups: Groups,
}

#[derive(Serialize, Deserialize)]
pub struct Groups {
    #[serde(rename = "backbone")]
    pub backbone: Backbone,
}

#[derive(Serialize, Deserialize)]
pub struct Backbone {
    #[serde(rename = "peers")]
    pub peers: HashMap<String, Option<Peer>>,

    #[serde(rename = "groups")]
    pub groups: Option<serde_json::Value>,
}

#[derive(Serialize, Deserialize)]
pub struct Peer {
    #[serde(rename = "established")]
    pub established: f64,
}

#[derive(Serialize, Deserialize)]
pub struct Processes {
    #[serde(rename = "total")]
    pub total: i64,

    #[serde(rename = "running")]
    pub running: i64,
}

#[derive(Serialize, Deserialize)]
pub struct Traffic {
    #[serde(rename = "tx")]
    pub tx: Tx,

    #[serde(rename = "rx")]
    pub rx: Forward,

    #[serde(rename = "forward")]
    pub forward: Forward,

    #[serde(rename = "mgmt_tx")]
    pub mgmt_tx: Forward,

    #[serde(rename = "mgmt_rx")]
    pub mgmt_rx: Forward,
}

#[derive(Serialize, Deserialize)]
pub struct Forward {
    #[serde(rename = "bytes")]
    pub bytes: Option<f64>,

    #[serde(rename = "packets")]
    pub packets: Option<i64>,
}

#[derive(Serialize, Deserialize)]
pub struct Tx {
    #[serde(rename = "bytes")]
    pub bytes: f64,

    #[serde(rename = "packets")]
    pub packets: i64,

    #[serde(rename = "dropped")]
    pub dropped: i64,
}