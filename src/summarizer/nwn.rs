use crate::model::{graph::GraphDocument, nodes::NodesDocument, summary::NodeWithoutNeighbor};
use crate::ConfyConfig;

use csv::Writer;
use std::error::Error;
use std::fs;
use std::fs::OpenOptions;

#[derive(Debug, PartialEq)]
pub enum SummarizeState {
    CheckConfig,
    ParseNodesJson,
    ParseGraphJson,
    Summarize,
    WriteSummary,
    Error,
    Finish,
}

#[derive(Debug, Clone, Copy)]
pub enum SummarizeEvent {
    CheckConfigFinished,
    ParseNodesJsonFinished,
    ParseGraphJsonFinished,
    SummarizeFinished,
    WriteSummaryFinished,
    SummaryFinished,
}
/** Neighbour without nodes Summarizer */
pub struct NeighbourWithoutNodesSummarizer {
    config: ConfyConfig,
    state: SummarizeState,
    nodes_document: Option<NodesDocument>,
    graph_document: Option<GraphDocument>,
    standalone_nodes: Vec<NodeWithoutNeighbor>,
}

impl NeighbourWithoutNodesSummarizer {
    pub fn new(input_config: ConfyConfig) -> Self {
        NeighbourWithoutNodesSummarizer {
            config: input_config,
            state: SummarizeState::CheckConfig,
            nodes_document: None,
            graph_document: None,
            standalone_nodes: Vec::new(),
        }
    }

    pub fn summarize(&mut self) {
        let events = vec![
            SummarizeEvent::CheckConfigFinished,
            SummarizeEvent::ParseGraphJsonFinished,
            SummarizeEvent::ParseNodesJsonFinished,
            SummarizeEvent::SummarizeFinished,
            SummarizeEvent::WriteSummaryFinished,
            SummarizeEvent::SummaryFinished,
        ];

        self.handle_events(events);
    }

    fn analyze(&mut self) -> std::result::Result<(), String> {
        // Analyze nodes without a neighbor
        match &self.nodes_document {
            Some(nd) => {
                match &self.graph_document {
                    Some(gd) => {
                        for node in nd.nodes.iter() {
                            let mut found: bool = false;

                            for graph_node in gd.batadv.nodes.iter() {
                                if graph_node.node_id == node.nodeinfo.node_id {
                                    found = true;
                                    break;
                                }
                            }

                            // possible candidate for node without neighbors...
                            if found == false {
                                if node.nodeinfo.network.addresses.len() == 2 {
                                    self.standalone_nodes.push(NodeWithoutNeighbor {
                                        hostname: node.nodeinfo.hostname.clone(),
                                        node_id: node.nodeinfo.node_id.clone(),
                                        address1: node.nodeinfo.network.addresses[0].clone(),
                                        address2: node.nodeinfo.network.addresses[1].clone(),
                                        release_firmware: node
                                            .nodeinfo
                                            .software
                                            .firmware
                                            .release
                                            .clone(),
                                        base_firmeware: node
                                            .nodeinfo
                                            .software
                                            .firmware
                                            .base
                                            .clone(),
                                        hardware: node.nodeinfo.hardware.model.clone(),
                                    });
                                } else if node.nodeinfo.network.addresses.len() == 1 {
                                    self.standalone_nodes.push(NodeWithoutNeighbor {
                                        hostname: node.nodeinfo.hostname.clone(),
                                        node_id: node.nodeinfo.node_id.clone(),
                                        address1: node.nodeinfo.network.addresses[0].clone(),
                                        address2: "".to_string(),
                                        release_firmware: node
                                            .nodeinfo
                                            .software
                                            .firmware
                                            .release
                                            .clone(),
                                        base_firmeware: node
                                            .nodeinfo
                                            .software
                                            .firmware
                                            .base
                                            .clone(),
                                        hardware: node.nodeinfo.hardware.model.clone(),
                                    });
                                } else if node.nodeinfo.network.addresses.len() == 0 {
                                    self.standalone_nodes.push(NodeWithoutNeighbor {
                                        hostname: node.nodeinfo.hostname.clone(),
                                        node_id: node.nodeinfo.node_id.clone(),
                                        address1: "".to_string(),
                                        address2: "".to_string(),
                                        release_firmware: node
                                            .nodeinfo
                                            .software
                                            .firmware
                                            .release
                                            .clone(),
                                        base_firmeware: node
                                            .nodeinfo
                                            .software
                                            .firmware
                                            .base
                                            .clone(),
                                        hardware: node.nodeinfo.hardware.model.clone(),
                                    });
                                }
                            }
                        }
                    }
                    None => return Err("Could not parde GraphDocument".to_string()),
                }
            }
            None => return Err("Could not parse Nodesdocument".to_string()),
        }
        Ok(())
    }

    fn write_csv(&self, filename: String) -> Result<(), Box<dyn Error>> {
        let file = OpenOptions::new()
            .write(true)
            .create(true)
            .append(true)
            .open(filename)
            .unwrap();
        let mut wtr = Writer::from_writer(file);

        for node in self.standalone_nodes.iter() {
            wtr.serialize(node)?;
        }

        wtr.flush()?;
        Ok(())
    }

    ///handle each separate event
    fn handle_event(&mut self, event: &SummarizeEvent) {
        // State: CheckConfig
        //Checking the config file, if the config isn't correct the process is aborted
        self.state = match (&self.state, event) {
            (SummarizeState::CheckConfig, SummarizeEvent::CheckConfigFinished) => {
                println!("Checking Config");
                if self.config.graph_uri.is_empty() == false
                    && self.config.nodes_uri.is_empty() == false
                {
                    SummarizeState::ParseGraphJson
                } else {
                    println!("Error in Config");
                    SummarizeState::Error
                }
            }
            (SummarizeState::ParseGraphJson, SummarizeEvent::ParseGraphJsonFinished) => {
                println!("Parse Graph Json");
                let gj = fs::read_to_string(self.config.local_graph_uri.clone());
                match gj {
                    Ok(json) => {
                        self.graph_document = serde_json::from_str(&json).unwrap();
                        SummarizeState::ParseNodesJson
                    }
                    Err(err) => {
                        println!("Could not read Nodes.json: {}", err);
                        SummarizeState::Error
                    }
                }
            }
            (SummarizeState::ParseNodesJson, SummarizeEvent::ParseNodesJsonFinished) => {
                println!("Parse Nodes Json");
                let nj = fs::read_to_string(self.config.local_nodes_uri.clone());
                match nj {
                    Ok(json) => {
                        self.nodes_document = serde_json::from_str(&json).unwrap();
                        SummarizeState::Summarize
                    }
                    Err(err) => {
                        println!("Could not read Nodes.json: {}", err);
                        SummarizeState::Error
                    }
                }
            }
            (SummarizeState::Summarize, SummarizeEvent::SummarizeFinished) => {
                println!("Analyze Data");
                match self.analyze() {
                    Ok(_) => SummarizeState::WriteSummary,
                    Err(err) => {
                        println!("Error in SummarizeState: {}", err);
                        SummarizeState::Error
                    }
                }
            }
            (SummarizeState::WriteSummary, SummarizeEvent::WriteSummaryFinished) => {
                println!("Write Csv");
                match self.write_csv("./output/nodes_without_neighbors.csv".to_string()) {
                    Ok(_) => SummarizeState::Finish,
                    Err(err) => {
                        println!("Error writing csv...");                    
                        println!("{}",err);
                        SummarizeState::Error
                    }
                };
                SummarizeState::Finish
            }
            (SummarizeState::Finish, SummarizeEvent::SummaryFinished) => {
                println!("Summary finished");
                SummarizeState::Finish
            }
            _=>{
                println!("Analyzing NodesWithoutNeighbours aborted");
                println!("{:?}",self.state);
                SummarizeState::Error
            }
        }
    }

    /// handle all events that exists...
    fn handle_events(&mut self, events: Vec<SummarizeEvent>) {
        for event in events.iter() {
            self.handle_event(&event);
        }
    }

    /// returns the new config
    pub fn get_config(self) -> ConfyConfig {
        self.config
    }
}
