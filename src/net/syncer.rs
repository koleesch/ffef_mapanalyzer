use crate::config::config::ConfyConfig;
use reqwest;
use std::fs;

#[derive(Debug, PartialEq)]
enum SyncState {
    CheckConfig,
    GetNodesJson,
    WriteNodesJson(String),
    GetGraphJson,
    WriteGraphJson(String),
    Error,
    Finish,
}

#[derive(Debug, Clone, Copy)]
enum SyncEvent {
    CheckConfigFinished,
    GetNodesJsonFinished,
    WriteNodesJsonFinished,
    GetGraphJsonFinished,
    WriteGraphJsonFinished,
    SyncFinished,
}

pub struct Syncer {
    config: ConfyConfig,
    state: SyncState,
}

impl Syncer {
    pub fn new(input_config: ConfyConfig) -> Self {
        Syncer {
            config: input_config,
            state: SyncState::CheckConfig,
        }
    }

    /// handle all events that exists...
    fn handle_events(&mut self, events: Vec<SyncEvent>) {
        for event in events.iter() {
            self.handle_event(&event);
        }
    }

    ///handle each separate event
    fn handle_event(&mut self, event: &SyncEvent) {
        // State: CheckConfig
        //Checking the config file, if the config isn't correct the process is aborted
        self.state = match (&self.state, event) {
            (SyncState::CheckConfig, SyncEvent::CheckConfigFinished) => {
                println!("Checking Config");
                if self.config.graph_uri.is_empty() == false
                    && self.config.nodes_uri.is_empty() == false
                {
                    SyncState::GetNodesJson
                } else {
                    println!("Error in Config");
                    SyncState::Error
                }
            }
            // State: GetNodesJson
            // Here the Nodes.json is downloaded, if an error occurs the process is aborted
            (SyncState::GetNodesJson, SyncEvent::GetNodesJsonFinished) => {
                println!("Getting Nodes from {}", self.config.nodes_uri);
                match self.get_json_from_uri(&self.config.nodes_uri.clone()) {
                    Ok(json) => {
                        println!("Sync successful");
                        SyncState::WriteNodesJson(json.clone())
                    }
                    Err(err) => {
                        println!("Error getting nodes {}", err);
                        SyncState::Error
                    }
                }
            }
            // State: WriteNodesJson
            // Here the correct parsed Nodes.Json will be backuped, if an error occurs, the process is aborted
            (SyncState::WriteNodesJson(json), SyncEvent::WriteNodesJsonFinished) => {
                println!("Write Nodes Document");
                match fs::create_dir("./output") {
                    Ok(_) => {}
                    Err(err) => match err.raw_os_error() {
                        Some(val) => {
                            // 17 means directory already exists
                            if val != 17 {
                                println!("Error creating output directory: {}", err);
                            }
                        }
                        None => {}
                    },
                }
                self.config.local_nodes_uri =
                    format!("./output/{}_nodes.json", self.config.last_nodes_sync);
                match fs::write(self.config.local_nodes_uri.clone(), json) {
                    Ok(_) => self.config.local_nodes_uri = self.config.local_nodes_uri.clone(),
                    Err(err) => {
                        self.config.local_nodes_uri = "".to_string();
                        println!("Error in writing nodes file - error {}", err);
                    }
                }                
                SyncState::GetGraphJson
            }

            //State: GetGraphjson
            // Getting the Graph.json from uri
            (SyncState::GetGraphJson, SyncEvent::GetGraphJsonFinished) => {
                println!("Getting Graph from {}", self.config.graph_uri);
                match self.get_json_from_uri(&self.config.graph_uri.clone()) {
                    Ok(json) => SyncState::WriteGraphJson(json),
                    Err(err) => {
                        println!("Error getting graph {}", err);
                        SyncState::Error
                    }
                }
            }
           
            // State: WriteGraph.json
            // Write the Graph.json to a file in the output directory
            (SyncState::WriteGraphJson(json), SyncEvent::WriteGraphJsonFinished) => {
                println!("Write Graph Document");
                match fs::create_dir("./output") {
                    Ok(_) => {}
                    Err(err) => match err.raw_os_error() {
                        Some(val) => {
                            // 17 means directory already exists
                            if val != 17 {
                                println!("Error creating output directory: {}", err);
                            }
                        }
                        None => {}
                    },
                }
                self.config.local_graph_uri =
                    format!("./output/{}_graph.json", self.config.last_graph_sync).to_string();
                println!("filename: {}", self.config.local_graph_uri);
                match fs::write(self.config.local_graph_uri.clone(), json) {
                    Ok(_) => self.config.local_graph_uri = self.config.local_graph_uri.clone(),
                    Err(err) => {
                        self.config.local_graph_uri = "".to_string();
                        println!("Error in writing nodes file - error {}", err)
                    }
                }
                SyncState::Finish
            }
            // TODO: Create a Cleanup State to delete Files that are more than 5 of Graph or Node in the output directory

            // State: Finished Sync
            (SyncState::Finish, SyncEvent::SyncFinished) => {
                println!("Sync finished");
                SyncState::Finish
            }

            // State: Error and aborted Sync
            _ => {
                println!("Sync aborted");
                SyncState::Error
            }
        }
    }

    /// sync
    /// creates all SynEvents and start the work on the statemachine
    pub fn sync(&mut self) {
        let events = vec![
            SyncEvent::CheckConfigFinished,
            SyncEvent::GetNodesJsonFinished,
            SyncEvent::WriteNodesJsonFinished,
            SyncEvent::GetGraphJsonFinished,
            SyncEvent::WriteGraphJsonFinished,
            SyncEvent::SyncFinished,
        ];

        self.handle_events(events);
    }

    /// returns the new config
    pub fn get_config(self) -> ConfyConfig {
        self.config
    }

    /// get_json_from_uri
    /// getting a json from the given uri
    /// the result is only filled, if the status is_success (http-status 2**)
    fn get_json_from_uri(&mut self, uri: &str) -> Result<String, Box<dyn std::error::Error>> {
        let resp = reqwest::blocking::get(uri)?;
        if resp.status().is_success() {
            let body = resp.text()?;
            return Ok(body);
        }

        Err(Box::new(resp.error_for_status().unwrap_err()))
    }
}
