#[macro_use]
extern crate serde_derive;

extern crate confy;

mod config;
mod model;
mod net;
mod summarizer;

use config::config::ConfyConfig;
use summarizer::nwn::NeighbourWithoutNodesSummarizer;

use net::syncer::Syncer;

fn main() -> Result<(), confy::ConfyError> {
    let mut cfg: ConfyConfig = confy::load_path("./config/config.toml")?;
    println!("{:#?}", cfg);

    let mut syncer: Syncer = Syncer::new(cfg);
    syncer.sync();
    cfg = syncer.get_config();

    let mut nwns:NeighbourWithoutNodesSummarizer = NeighbourWithoutNodesSummarizer::new(cfg);
    nwns.summarize();
    //println!("{:#?}", cfg);
    cfg = nwns.get_config();
    confy::store_path("./config/config.toml", cfg)?;

    Ok(())
}