use ::std::default::Default;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ConfyConfig {
    pub nodes_uri: String,
    pub graph_uri: String,
    pub last_nodes_sync: String,
    pub last_graph_sync: String,
    pub local_nodes_uri: String,
    pub local_graph_uri: String,
}

/// Default implementation
impl Default for ConfyConfig {
    fn default() -> Self {
        ConfyConfig {            
            nodes_uri: "https://map.erfurt.freifunk.net/meshviewer/nodes.json".into(),
            graph_uri: "https://map.erfurt.freifunk.net/meshviewer/graph.json".into(),
            last_nodes_sync: chrono::Utc::now().to_rfc3339(),
            last_graph_sync: chrono::Utc::now().to_rfc3339(),
            local_nodes_uri: "".into(),
            local_graph_uri: "".into(),
        }
    }
}
