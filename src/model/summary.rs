#[derive(Serialize, Deserialize,Debug, PartialEq)]
pub struct NodeWithoutNeighbor {
    pub hostname: String,
    pub node_id: String,
    pub address1: String,
    pub address2: String,
    pub release_firmware: String,
    pub base_firmeware: String,
    pub hardware: String,
}