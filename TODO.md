# Actually Todo

1. [X] Analyze the synced data to find standalone nodes
2. [] Write a csv/toml file for standalone nodes
3. [X] Change the state machine model in syncer.rs (Check config, getjson, writejson, parsejson)
4. [] delete synced files in output-directory after 5 entries (oldest first)
5. [X] create documentation
6. [] Rework of CSV-Implementation (Encapsulation)
7. [X] Rework of SyncStates -> Parsing is not necessary